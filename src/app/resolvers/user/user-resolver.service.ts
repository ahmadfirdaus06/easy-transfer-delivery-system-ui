import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiServiceService } from 'src/app/api/api-service.service';
import { User } from 'src/app/models/user';

@Injectable({
  providedIn: 'root'
})
export class UserResolverService implements Resolve<User[]> {

  constructor(
    private apiService: ApiServiceService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): 
  Observable<User[]> | Promise<User[]> | User[]{
    return this.apiService.getUserData();
  }
}
