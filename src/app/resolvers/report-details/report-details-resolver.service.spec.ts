import { TestBed } from '@angular/core/testing';

import { ReportDetailsResolverService } from './report-details-resolver.service';

describe('ReportDetailsResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReportDetailsResolverService = TestBed.get(ReportDetailsResolverService);
    expect(service).toBeTruthy();
  });
});
