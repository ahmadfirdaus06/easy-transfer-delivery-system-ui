import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Report } from 'src/app/models/report';
import { ApiServiceService } from 'src/app/api/api-service.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReportDetailsResolverService implements Resolve<Report[]>{

  constructor(
    private apiService: ApiServiceService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): 
  Observable<User[]> | Promise<User[]> | User[]{
    return this.apiService.getReportDetails();
  }
}
