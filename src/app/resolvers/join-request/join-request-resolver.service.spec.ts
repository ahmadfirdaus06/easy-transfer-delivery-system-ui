import { TestBed } from '@angular/core/testing';

import { JoinRequestResolverService } from './join-request-resolver.service';

describe('JoinRequestResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JoinRequestResolverService = TestBed.get(JoinRequestResolverService);
    expect(service).toBeTruthy();
  });
});
