import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { User } from 'src/app/models/user';
import { ApiServiceService } from 'src/app/api/api-service.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JoinRequestResolverService implements Resolve<User[]>{

  constructor(
    private apiService: ApiServiceService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): 
  Observable<User[]> | Promise<User[]> | User[]{
    return this.apiService.getJoinRequests(null);
  }
}
