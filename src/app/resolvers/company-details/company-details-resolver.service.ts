import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Company } from 'src/app/models/company';
import { ApiServiceService } from 'src/app/api/api-service.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CompanyDetailsResolverService implements Resolve<Company[]>{

  constructor(
    private apiService: ApiServiceService,
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): 
  Observable<Company[]> | Promise<Company[]> | Company[]{
    return this.apiService.getCompanyDetails();
  }
}
