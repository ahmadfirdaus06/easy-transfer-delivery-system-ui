import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { User } from 'src/app/models/user';
import { ApiServiceService } from 'src/app/api/api-service.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StaffDetailsResolverService implements Resolve<User[]>{

  constructor(
    private apiService: ApiServiceService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): 
  Observable<User[]> | Promise<User[]> | User[]{
    return this.apiService.getStaffDetails(null);
  }
}
