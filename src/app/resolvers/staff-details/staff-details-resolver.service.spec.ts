import { TestBed } from '@angular/core/testing';

import { StaffDetailsResolverService } from './staff-details-resolver.service';

describe('StaffDetailsResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StaffDetailsResolverService = TestBed.get(StaffDetailsResolverService);
    expect(service).toBeTruthy();
  });
});
