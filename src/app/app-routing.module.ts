import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { MainComponent } from './pages/main/main.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ReportComponent } from './pages/report/report.component';
import { StockAndInventoryComponent } from './pages/stock-and-inventory/stock-and-inventory.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { UserResolverService } from './resolvers/user/user-resolver.service';
import { ProfileComponent } from './pages/profile/profile.component';
import { CompanyComponent } from './pages/company/company.component';
import { StaffComponent } from './pages/staff/staff.component';
import { CompanyDetailsResolverService } from './resolvers/company-details/company-details-resolver.service';
import { StaffListComponent } from './pages/staff-list/staff-list.component';
import { JoinRequestComponent } from './pages/join-request/join-request.component';
import { StaffDetailsResolverService } from './resolvers/staff-details/staff-details-resolver.service';
import { JoinRequestResolverService } from './resolvers/join-request/join-request-resolver.service';
import { StaffDetailsComponent } from './pages/staff-details/staff-details.component';
import { JoinRequestDetailsComponent } from './pages/join-request-details/join-request-details.component';
import { ReportDetailsResolverService } from './resolvers/report-details/report-details-resolver.service';
import { OrderDetailsComponent } from './pages/order-details/order-details.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'main',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'main',
    component: MainComponent,
    resolve: {
      user: UserResolverService,
      companyDetails: CompanyDetailsResolverService
    },
    children: [
      {
        path:'',
        redirectTo: 'dashboard',
        pathMatch: 'full' 
    },
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'report',
        component: ReportComponent,
        resolve: {
          reportDetails : ReportDetailsResolverService
        }
      },
      {
        path: 'stock-and-inventory',
        component: StockAndInventoryComponent
      },
      {
        path: 'profile',
        component: ProfileComponent,
        resolve: {
          user: UserResolverService
        }
      },
      {
        path: 'staff',
        component: StaffComponent,
        resolve: {
          staffDetails: StaffDetailsResolverService,
          joinRequests: JoinRequestResolverService
        },
        children: [
          {
            path: '',
            redirectTo: 'staff-list',
            pathMatch: 'full' 
          },
          {
            path: 'staff-list',
            component: StaffListComponent,
            resolve: {
              staffDetails: StaffDetailsResolverService
            }
          },
          {
            path: 'join-request',
            component: JoinRequestComponent,
            resolve: {
              joinRequests: JoinRequestResolverService
            }
          }
        ]
      },
      {
        path: 'company',
        component: CompanyComponent,
        resolve: {
          companyDetails: CompanyDetailsResolverService
        }
      },
      {
        path: 'staff-details/:id',
        component: StaffDetailsComponent
      },
      {
        path: 'join-request-details/:id',
        component: JoinRequestDetailsComponent
      },
      {
        path: 'order-details/:id',
        component: OrderDetailsComponent
      },
    ]
  },
  {
    path: '404',
    component: PageNotFoundComponent
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
