import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialog: MatDialogRef<ConfirmDialogComponent>
  ) { }

  ngOnInit() {
  }

  yes():void{
    this.dialog.close({
      confirm: true
    });
  }

  no():void{
    this.dialog.close({
      confirm: false
    });
  }

}
