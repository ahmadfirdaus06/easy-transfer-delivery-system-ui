import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { ApiServiceService } from '../../api/api-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { SimpleSnackBar, MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  constructor(
    private http:HttpClient,
    private apiService: ApiServiceService,
    private router: Router,
    private location: Location,
    private toast: MatSnackBar,
    private route: ActivatedRoute
    ) { }

    @ViewChild('sidenav', {static: false}) sidenav: ElementRef;

    TOAST_CONFIG = {
      duration: 3000,
      panelClass: ['font-weight-bold', 'text-white']
    }

    mobile = false;

  ngAfterViewInit(){
    if (document.documentElement.clientWidth <= 400){
      this.mobile = true;
    }
  }

  ngOnInit() {
    this.route.data.subscribe(
      (response) => {
        this.user = response.user.data;
        this.companyDetails.push(response.companyDetails.data);
      },
      (error) => {
        console.log(error);
    });
    
    this.companyName = this.companyDetails[0].company_name;
    var matches = this.companyName.match(/\b(\w)/g);
    this.abbrCompanyName = matches.join('');
  }

  companyName = '';
  abbrCompanyName = ''; 
  user = '';
  companyDetails = [];

  logout():void{
    sessionStorage.clear();
    if (sessionStorage.length == 0){
      this.router.navigate(['/login'])
      .then(() => {
        window.location.reload()
      });
      this.location.replaceState('/login');
      this.toast.open('Logout Success', 'HIDE', this.TOAST_CONFIG);
    }
  }

}
