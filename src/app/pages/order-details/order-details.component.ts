import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiServiceService } from 'src/app/api/api-service.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit {

  orderDetailsForm: FormGroup;
  submitted = false;

  constructor(
    private route: ActivatedRoute,
    private apiService: ApiServiceService,
    private formBuilder: FormBuilder,
    private router: Router,
    private location: Location
  ) { }

  orderDetails = [];

  ngOnInit() {
    let params = {
      order_id: this.route.snapshot.paramMap.get('id')
    }

    this.apiService.getOrderDetails(params).subscribe(
      (response) => {
        this.orderDetails.push(response);
        if (this.orderDetails[0].status){  
          this.orderDetails = this.orderDetails[0].data[0];
          console.log(this.orderDetails);
          this.orderDetailsForm = this.formBuilder.group({
            order_id: [this.orderDetails.id],
            created_at: [this.orderDetails.created_at],
            pickup_date_time: [this.orderDetails.pickup_date_time],
            dropoff_date_time: [this.orderDetails.dropoff_date_time],
            pickup_location: [this.orderDetails.pickup_location],
            dropoff_location: [this.orderDetails.dropoff_location],
            delivery_distance: [this.orderDetails.delivery_distance],
            order_accept_by: [this.orderDetails.order_accept_by],
            parcel_size_desc: [this.orderDetails.parcel_size_desc],
            payment_method_desc: [this.orderDetails.payment_method_desc],
            total_cost_delivery: [this.orderDetails.total_cost_delivery],
            customer_name: [this.orderDetails.customer_name],
            customer_phone_num: [this.orderDetails.customer_phone_num],
            driver_name: [this.orderDetails.driver_name],
            driver_phone_num: [this.orderDetails.driver_phone_num]
          });
        }
        else{
          this.router.navigate(['/404']);
          this.location.replaceState('/404');
        }
      },
      (error) => {
        console.log(error);
      }
    );

  }

}
