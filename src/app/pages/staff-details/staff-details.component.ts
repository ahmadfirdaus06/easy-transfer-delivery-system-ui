import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiServiceService } from 'src/app/api/api-service.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Location } from '@angular/common';

@Component({
  selector: 'app-staff-details',
  templateUrl: './staff-details.component.html',
  styleUrls: ['./staff-details.component.scss']
})
export class StaffDetailsComponent implements OnInit {

  editStaffProfileForm: FormGroup;
  submitted = false;
  passwordConfirmed = true;

  constructor(
    private route: ActivatedRoute,
    private apiService: ApiServiceService,
    private formBuilder: FormBuilder,
    private router: Router,
    private location: Location
  ) { }

  staffDetails = [];
  statusDesc = [];

  ngOnInit() {
    let params = {
      id: this.route.snapshot.paramMap.get('id')
    }
    this.apiService.getStaffDetails(params).subscribe(
      (response) => {
        this.staffDetails.push(response);
        if (this.staffDetails[0].status){
          params = {
            id: this.staffDetails[0].data.user_status_id
          }
          this.apiService.getUserStatus(params).subscribe(
            (response) =>{
              this.statusDesc.push(response);
              this.statusDesc = this.statusDesc[0].data.desc;
              this.editStaffProfileForm = this.formBuilder.group({
                id: [this.staffDetails[0].data.id],
                name: [this.staffDetails[0].data.name, Validators.required],
                nric: [this.staffDetails[0].data.nric, Validators.required],
                created_at: [this.staffDetails[0].data.created_at],
                updated_at: [this.staffDetails[0].data.updated_at],
                username: [this.staffDetails[0].data.username, Validators.required],
                email: [this.staffDetails[0].data.email, Validators.required],
                phone_num: [this.staffDetails[0].data.phone_num, Validators.required],
                user_status_id: [this.staffDetails[0].data.user_status_id],
                status_desc: [this.statusDesc.toString().charAt(0).toUpperCase() + this.statusDesc.slice(1)],
                new_password: [''],
                password: [''],
              });
            },
            (error) =>{
              console.log(error);
            });
        }
        else{
          this.router.navigate(['/404']);
          this.location.replaceState('/404');
        } 
    },
    (error) => {
      console.log(error);
    });
  }

  validatePassword():Boolean{
    const newPassword = this.editStaffProfileForm.value.new_password;
    const confirmPassword = this.editStaffProfileForm.value.password;

    if (newPassword != "" && confirmPassword != ""){
      if (newPassword == confirmPassword){
        delete this.editStaffProfileForm.value.new_password;
        this.passwordConfirmed = true;
        return true;
      }
      else{
        this.passwordConfirmed = false;
        return false;
      }
    }
    else if (newPassword != "" || confirmPassword != ""){
      this.passwordConfirmed = false;
      return false;
    }
    else if (newPassword == "" && confirmPassword == ""){
      delete this.editStaffProfileForm.value.password;
      delete this.editStaffProfileForm.value.new_password;
      this.passwordConfirmed = true;
      return true;
    }
  }

  update(){
    this.submitted = true;
    
    if (!this.editStaffProfileForm.invalid && this.validatePassword()) {
      delete this.editStaffProfileForm.value.status_desc;
      const params = this.editStaffProfileForm.value;
      this.apiService.updateUserDetails(params);
    }
    else{
      return;
    }
  }

}
