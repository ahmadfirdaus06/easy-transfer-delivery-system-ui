import { Component, OnInit, Directive, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {HttpClient, HttpParams} from "@angular/common/http";
import { ApiServiceService } from "../../api/api-service.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

@Directive({
  selector: '[autofocus]'
})

export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder, 
    private http:HttpClient,
    private apiService: ApiServiceService,
    private focusField: ElementRef,
    ) { }

  ngAfterViewInit(){
    this.focusField.nativeElement.focus();
  }

  ngOnInit() {

    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    
    });

    this.apiService.checkSession();
  }
  onSubmit() {
    this.submitted = true;

    // stop the process here if form is invalid
    if (!this.loginForm.invalid) {

      const params = {
        username: this.loginForm.value.username,
        password: this.loginForm.value.password,
        user_type_id: '1'
      }

      this.apiService.login(params);  

    }
    else{
      return;
    }
    
}

}
