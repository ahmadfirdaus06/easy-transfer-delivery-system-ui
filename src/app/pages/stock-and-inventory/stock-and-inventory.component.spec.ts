import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockAndInventoryComponent } from './stock-and-inventory.component';

describe('StockAndInventoryComponent', () => {
  let component: StockAndInventoryComponent;
  let fixture: ComponentFixture<StockAndInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockAndInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockAndInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
