import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiServiceService } from 'src/app/api/api-service.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Location } from '@angular/common';

@Component({
  selector: 'app-join-request-details',
  templateUrl: './join-request-details.component.html',
  styleUrls: ['./join-request-details.component.scss']
})
export class JoinRequestDetailsComponent implements OnInit {

  joinRequestDetailsForm: FormGroup;
  submitted = false;

  constructor(
    private route: ActivatedRoute,
    private apiService: ApiServiceService,
    private formBuilder: FormBuilder,
    private router: Router,
    private location: Location
  ) { }

  joinRequestDetails = [];
  requestDetails = null;

  ngOnInit() {
    let params = {
      id: this.route.snapshot.paramMap.get('id')
    }

    this.apiService.getJoinRequests(params).subscribe(
      (response) => {
        this.joinRequestDetails.push(response);
        if (this.joinRequestDetails[0].status){
          this.requestDetails = this.joinRequestDetails[0].data[0];
          this.joinRequestDetailsForm = this.formBuilder.group({
            username: [this.requestDetails.username],
            name: [this.requestDetails.name],
            nric: [this.requestDetails.nric],
            email: [this.requestDetails.email],
            phone_num: [this.requestDetails.phone_num],
            created_at: [this.requestDetails.created_at],
          });
        }
        else{
          this.router.navigate(['/404']);
          this.location.replaceState('/404');
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  update(applicationStatus):void{
    this.requestDetails.application_status = applicationStatus;
    delete this.requestDetails.created_at;
    
    const params = this.requestDetails;
    
    this.apiService.updateJoinRequestApproval(params);
  }

}
