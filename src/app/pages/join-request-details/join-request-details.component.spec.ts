import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinRequestDetailsComponent } from './join-request-details.component';

describe('JoinRequestDetailsComponent', () => {
  let component: JoinRequestDetailsComponent;
  let fixture: ComponentFixture<JoinRequestDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoinRequestDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JoinRequestDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
