import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  editUserProfileForm: FormGroup;
  submitted = false;

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
  ) 
  { }

  ngOnInit() {
    this.route.data.subscribe(
      (response) => {
        this.user.push(response.user.data);
        this.editUserProfileForm = this.formBuilder.group({
          id: [this.user[0].id, Validators.required],
          name: [this.user[0].name, Validators.required],
          nric: [this.user[0].nric, Validators.required],
          last_login: [this.user[0].last_login, Validators.required],
          username: [this.user[0].username, Validators.required],
          email: [this.user[0].email, Validators.required],
          phone_num: [this.user[0].phone_num, Validators.required],
          new_password: ['', Validators.required],
          password: ['', Validators.required],
        });
      },
      (error) => {
        console.log(error);
      });
  }

  user = [];

}
