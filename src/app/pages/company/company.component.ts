import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiServiceService } from '../../api/api-service.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {

  editCompanyForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder, 
    private apiService: ApiServiceService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.data.subscribe(
      (response) => {
        this.companyDetails.push(response.companyDetails.data);
        this.editCompanyForm = this.formBuilder.group({
          company_name: [this.companyDetails[0].company_name, Validators.required],
          desc: [this.companyDetails[0].desc, Validators.required],
          about: [this.companyDetails[0].about, Validators.required],
          establish_at: [this.companyDetails[0].establish_at, Validators.required],
          updated_at: [this.companyDetails[0].updated_at, Validators.required]
        });
      },
      (error) => {
        console.log(error);
      });
  }

  companyDetails = [];

  update(){
    this.submitted = true;

    if (!this.editCompanyForm.invalid) {

      const params = {
        company_name: this.editCompanyForm.value.company_name,
        desc: this.editCompanyForm.value.desc,
        about: this.editCompanyForm.value.about,
        establish_at: this.editCompanyForm.value.establish_at,
        id: "1"
      }

      this.apiService.updateCompanyDetails(params);

    }
    else{
      return;
    }
  }

}
