import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.scss']
})
export class StaffComponent implements OnInit {

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.data.subscribe(
      (response) => {
        this.staffCount = response.staffDetails.data.length;
        this.requestCount = response.joinRequests.data.length;
      },
      (error) => {
        console.log(error);
    });
  }

  staffCount = 0;
  requestCount = 0;

}
