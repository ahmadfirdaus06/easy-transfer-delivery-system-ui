import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { ApiServiceService } from 'src/app/api/api-service.service';

@Component({
  selector: 'app-staff-list',
  templateUrl: './staff-list.component.html',
  styleUrls: ['./staff-list.component.scss']
})
export class StaffListComponent implements OnInit {

  displayedColumns: string[] = ['no', 'name', 'username', 'created_at', 'updated_at', 'status', 'actions'];
  dataSource: MatTableDataSource<User>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private route: ActivatedRoute,
    private apiService: ApiServiceService
  ) {}

  ngOnInit() {
    this.dataSource = new MatTableDataSource();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.route.data.subscribe(
      (response) => {
        this.dataSource.data = response.staffDetails.data;
      },
      (error) => {
        console.log(error);
    });
  }

  changeStatus(e, user){
    let statusId = 0;

    if (e.checked){
      statusId = 1;
    }
    else{
      statusId = 2;
    }
    
    const params = user;

    delete params.password;
    delete params.new_password;
    
    params.user_status_id = statusId;
    this.apiService.updateUserDetails(params);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}

