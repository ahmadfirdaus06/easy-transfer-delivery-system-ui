import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { ReactiveFormsModule } from '@angular/forms';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSliderModule} from '@angular/material/slider';
import {MatSlideToggleModule, MatSlideToggle} from '@angular/material/slide-toggle';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatListModule} from '@angular/material/list';
import {MatDialogModule} from '@angular/material/dialog';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatInputModule} from '@angular/material';
import {MatSortModule} from '@angular/material/sort';
import {MatTooltipModule} from '@angular/material/tooltip';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { MainComponent } from './pages/main/main.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ReportComponent } from './pages/report/report.component';
import { StockAndInventoryComponent } from './pages/stock-and-inventory/stock-and-inventory.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { ApiServiceService } from './api/api-service.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { ProfileComponent } from './pages/profile/profile.component';
import {MatCardModule} from '@angular/material/card';
import { CompanyComponent } from './pages/company/company.component';
import { StaffComponent } from './pages/staff/staff.component';
import { ConfirmDialogComponent } from './dialog/confirm-dialog/confirm-dialog.component';
import { StaffListComponent } from './pages/staff-list/staff-list.component';
import { JoinRequestComponent } from './pages/join-request/join-request.component';
import { StaffDetailsComponent } from './pages/staff-details/staff-details.component';
import { LoadingDialogComponent } from './dialog/loading-dialog/loading-dialog.component';
import { JoinRequestDetailsComponent } from './pages/join-request-details/join-request-details.component';
import { OrderDetailsComponent } from './pages/order-details/order-details.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    DashboardComponent,
    ReportComponent,
    StockAndInventoryComponent,
    PageNotFoundComponent,
    ProfileComponent,
    StaffComponent,
    CompanyComponent,
    ConfirmDialogComponent,
    StaffListComponent,
    JoinRequestComponent,
    StaffDetailsComponent,
    LoadingDialogComponent,
    JoinRequestDetailsComponent,
    OrderDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatToolbarModule,
    HttpClientModule,
    MatButtonModule,
    MatListModule,
    MatSnackBarModule,
    MatCardModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatTabsModule,
    MatTableModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatInputModule,
    MatSortModule,
    MatTooltipModule
  ],
  entryComponents: [
    ConfirmDialogComponent,
    LoadingDialogComponent
  ],
  providers: [
    ApiServiceService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
export class MaterialModule{}
