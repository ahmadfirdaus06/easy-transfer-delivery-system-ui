import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user';
import { Company } from '../models/company';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../dialog/confirm-dialog/confirm-dialog.component';
import { LoadingDialogComponent } from '../dialog/loading-dialog/loading-dialog.component';
import { UserStatus } from '../models/user-status';
import { Report } from '../models/report';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  constructor(
    private http: HttpClient,
    private router: Router,
    private location: Location,
    private toast: MatSnackBar,
    private dialog: MatDialog,
    private route: ActivatedRoute
  ) { }

  TOAST_CONFIG = {
    duration: 3000,
    panelClass: ['font-weight-bold', 'text-white']
  }

  httpHeader = {
    headers : new HttpHeaders(
      {
        'Content-Type':'application/json; charset=utf-8',
        'Access-Control-Allow-Origin':'*'
      }
    )
  }

  confirm = null;
  loading = null;

  openConfirmDialog(desc): void{
    this.confirm = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      disableClose: true,
      data: {
        desc
      }
    });
  }

  openLoadingDialog(): void{
    this.loading = this.dialog.open(LoadingDialogComponent, {
      disableClose: true,
    });
  } 

  closeLoadingDialog(): void{
    if (this.loading != null){
      this.loading.close();
    }
  }

  reloadResolver() {
    // this.router.navigated = false;
    // this.router.navigate([this.route.url]);
    // console.log([this.router.url]);
    this.router.navigateByUrl(this.router.url);
  }

  reloadPage(){
    setTimeout(() => {
      window.location.reload();
    }, 1000);
  }

  result = [];

  login(params): void{
    this.openLoadingDialog();
    this.http.post(environment.API_URL + 'user/login.php', params, this.httpHeader).subscribe(
      (response) => {
        this.closeLoadingDialog();
        this.loading.afterClosed().subscribe(() => {
          this.result = [];
          this.result.push(response);
          if(this.result[0].status){
            sessionStorage.setItem('user_session', JSON.stringify(this.result[0].data));
            this.router.navigate(['/main/dashboard']);
            this.location.replaceState('/main/dashboard');
          }
          this.toast.open(this.result[0].message, 'HIDE', this.TOAST_CONFIG);
          });
    },
    (error) => {
      this.loading.afterClosed().subscribe(() => {
        console.log(error); 
      });
    });
  }

  checkSession():Boolean{
    if (sessionStorage.length == 0){
      this.router.navigate(['/login']);
      this.location.replaceState('/login');
      return false;
    }
    else{
      return true;
    }
  }

  getUserData():Observable<User[]>{
    if (this.checkSession()){
      return this.http.post<User[]>(environment.API_URL + 'user/get_user_data.php', sessionStorage.getItem('user_session'), this.httpHeader).pipe();
    }
    else{
      return null;
    }
  }

  getCompanyDetails(): Observable<Company[]>{
    return this.http.get<Company[]>(environment.API_URL + 'user/get_company_details.php', this.httpHeader).pipe();
  }

  updateCompanyDetails(params): void{
    this.openConfirmDialog('Changes will be saved!');
    this.confirm.afterClosed().subscribe((response) => {
      if (response.confirm){
        this.openLoadingDialog();
        this.http.post(environment.API_URL + 'user/update_company_details.php', params, this.httpHeader).subscribe(
          (response) => {
            this.closeLoadingDialog();
            this.loading.afterClosed().subscribe(() =>{
              this.result = [];
              this.result.push(response);
              this.toast.open(this.result[0].message, 'HIDE', this.TOAST_CONFIG);
              if(this.result[0].status){
                this.reloadPage();
              }
            });
        },
        (error) => {
          this.loading.close();
          this.loading.afterClosed().subscribe(() =>{
            console.log(error);
          });
          
        });
      }
    });
  }

  getJoinRequests(params): Observable<User[]>{
    if (params != null){
      return this.http.post<User[]>(environment.API_URL + 'get/staff_join_request.php', params, this.httpHeader).pipe();
    }
    else{
      return this.http.get<User[]>(environment.API_URL + 'get/staff_join_request.php', this.httpHeader).pipe();
    }
    
  }

  getStaffDetails(params): Observable<User[]>{
    if (params != null){
      return this.http.post<User[]>(environment.API_URL + 'user/get_staff_details.php', params, this.httpHeader).pipe();
    }
    else{
      return this.http.get<User[]>(environment.API_URL + 'user/get_staff_details.php', this.httpHeader).pipe();  
    }
  }

  updateUserDetails(params): void{
    this.openConfirmDialog('Changes will be saved!');
    this.confirm.afterClosed().subscribe((response) => {
      if (response.confirm){
        this.openLoadingDialog();
        this.http.put(environment.API_URL + 'update/user.php', params, this.httpHeader).subscribe(
          (response) => {
            this.closeLoadingDialog();
            this.loading.afterClosed().subscribe(() =>{
              this.result = [];
              this.result.push(response);
              this.toast.open(this.result[0].message, 'HIDE', this.TOAST_CONFIG);
              if(this.result[0].status){
                this.router.navigate(['/main/staff']);
                this.location.replaceState('/main/staff');
                this.reloadPage();
              }
            });
        },
        (error) => {
          this.loading.close();
          this.loading.afterClosed().subscribe(() =>{
            console.log(error);
          });
          
        });
      }else{
        this.reloadPage();
      }
    });
  }

  getUserStatus(params): Observable<UserStatus[]>{
    return this.http.post<UserStatus[]>(environment.API_URL + 'get/user_status.php', params, this.httpHeader).pipe();
  }

  updateJoinRequestApproval(params): void{
    this.openConfirmDialog('Changes will be saved!');
    this.confirm.afterClosed().subscribe((response) => {
      if (response.confirm){
        this.openLoadingDialog();
        this.http.post(environment.API_URL + 'create/staff.php', params, this.httpHeader).subscribe(
          (response) => {
            this.closeLoadingDialog();
            this.loading.afterClosed().subscribe(() =>{
              this.result = [];
              this.result.push(response);
              this.toast.open(this.result[0].message, 'HIDE', this.TOAST_CONFIG);
              if(this.result[0].status){
                this.router.navigate(['/main/staff']);
                this.location.replaceState('/main/staff');
                this.reloadPage();
              }
            });
        },
        (error) => {
          this.loading.close();
          this.loading.afterClosed().subscribe(() =>{
            console.log(error);
          });
          
        });
      }
    });
  }

  getReportDetails(): Observable<Report[]>{
    return this.http.get<Report[]>(environment.API_URL + 'get/report.php', this.httpHeader).pipe();
  }

  getOrderDetails(params): Observable<Order[]>{
    if (params != null){
      return this.http.post<Order[]>(environment.API_URL + 'get/customer_order.php', params, this.httpHeader).pipe();
    }
    
  }
}


